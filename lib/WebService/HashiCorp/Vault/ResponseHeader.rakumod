=begin pod
Copyright 2023 Dean Powell

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=end pod

class ResponseHeader {
    has Str $!requestId;
    has Str $!leaseId;
    has Bool $!renewable;
    has Int $!leaseDuration;

    multi method requestId {
        $!requestId;
    }

    multi method requestId(Str $aString) {
        $!requestId = $aString;
    }

    multi method leaseId {
        $!leaseId;
    }

    multi method leaseId(Str $aString) {
        $!leaseId = $aString;
    }

    multi method renewable {
        $!renewable;
    }

    multi method renewable($aBool) {
        $!renewable = $aBool;
    }

    multi method leaseDuration {
        $!leaseDuration;
    }

    multi method leaseDuration($aNumber) {
        $!leaseDuration = $aNumber;
    }

    submethod BUILD(
        :$!requestId='',
        :$!leaseId='',
        :$!renewable=False,
        :$!leaseDuration=0) {
    }

    method fromJson(:$json!) {
        self.requestId:     $json{'request_id'};
        self.leaseId:       $json{'lease_id'};
        self.renewable:     $json{'renewable'};
        self.leaseDuration: $json{'lease_duration'};

        self;
    }
}
